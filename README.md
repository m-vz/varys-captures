This repository contains all traffic traces captured by `varys` in `.xz`-compressed `.pcap` format.

To decompress everything, you can use `find . -name "*.pcap.xz" | xargs xz --decompress` in the sessions directory. This will take a while, since there are >70000 captures.